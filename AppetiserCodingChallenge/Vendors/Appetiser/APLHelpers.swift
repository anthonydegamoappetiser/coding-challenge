import UIKit

public struct APLHelpers {}

// MARK: - Threading
public extension APLHelpers {
  
  /// Performs an intensive process in the background, then calls the specified completion
  /// block, if provided, on the main thread.
  ///
  /// Ref: http://stackoverflow.com/a/39082295/425694
  static func performInBackground(_ work: @escaping () -> Void, completion: (() -> Void)? = nil) {
    DispatchQueue.global(qos: .background).async {
      work()
      DispatchQueue.main.async {
        completion?()
      }
    }
  }
  
  /// Ref: http://stackoverflow.com/a/24318861/425694
  /// Ref: http://stackoverflow.com/a/27462144/425694
  static func delay(_ seconds: Double, task: @escaping () -> ()) {
    let when = DispatchTime.now() + seconds
    DispatchQueue.main.asyncAfter(deadline: when, execute: task)
  }
  
}

// MARK: - File Management
public extension APLHelpers {
  
  static func documentsDirectoryURL() -> URL {
    return FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
  }
  
  static func sanitizedFileName(_ filename: String) -> String {
    return filename.components(separatedBy: .init(charactersIn: "/:?%*|\"<>")).joined()
  }
  
}

import MapKit

// MARK: - Maps
public extension APLHelpers {
  
  static func visibleMapRectForCoordinates(_ coordinates: [CLLocationCoordinate2D]) -> MKMapRect {
    let points = coordinates.map { MKMapPoint($0) }
    let rects = points.map { MKMapRect(origin: $0, size: MKMapSize(width: 0, height: 0)) }
    return rects.reduce(MKMapRect.null, { (res, rect) -> MKMapRect in
      return rect.union(res)
    })
  }
  
}
