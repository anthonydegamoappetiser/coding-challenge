//
//  Notification+Name.swift
//  AppetiserCodingChallenge
//
//  Created by Mark Anthony Degamo on 03/09/2019.
//  Copyright © 2019 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

extension Notification.Name {
  private static let baseKey = "AppetiserCodingChallenge.Notification.Name"

  /// Use this key to tell any observers that the `FavoriteStore` has been updated.
  static let onFavoriteStoreUpdated = Notification.Name("\(baseKey).onFavoriteStoreUpdated")
}
