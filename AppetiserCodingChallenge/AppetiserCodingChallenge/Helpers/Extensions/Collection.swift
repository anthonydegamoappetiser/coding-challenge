//
//  Collection.swift
//  AppetiserCodingChallenge
//
//  Created by "Team Appetiser" ( https://appetiser.com.au )
//  Copyright © 2019 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

extension Collection {
  
  /// Returns the element at the specified index if it is within bounds, otherwise nil
  ///
  /// Usage:
  ///     array[safe: 2]
  subscript(safe index: Index) -> Element? {
    return indices.contains(index) ? self[index] : nil
  }
  
}
