//
//  ViewController.swift
//  AppetiserCodingChallenge
//
//  Created by "Team Appetiser" ( https://appetiser.com.au )
//  Copyright © 2018 "Appetiser Pty Ltd". All rights reserved.
//

import UIKit

class ViewController: UIViewController {
  
  override func viewDidLoad() {
    super.viewDidLoad()
    view.backgroundColor = Styles.Colors.viewControllerBackground
    setupNavBarItems()
  }
  
  /// A place for adding your custom navigation bar buttons.
  ///
  /// By default, a custom **Back** button will appear on the left side of the navigation bar
  /// if this controller isn't the root of its `navigationController`. On the other hand,
  /// if it's presented modally, we show a **Close** button.
  ///
  /// This method is called inside `viewDidLoad`.
  ///
  func setupNavBarItems() {
    if navigationController?.viewControllers.first != self {
      navigationItem.leftBarButtonItem = UIBarButtonItem(
        title: S.back(),
        style: .plain,
        target: self,
        action: #selector(backButtonTapped(_:))
      )
    } else if isPresentedModally {
      navigationItem.leftBarButtonItem = UIBarButtonItem(
        title: S.close(),
        style: .plain,
        target: self,
        action: #selector(backButtonTapped(_:))
      )
    }
  }
  
  @IBAction
  func backButtonTapped(_ sender: AnyObject) {
    if isPresentedModally {
      dismiss(animated: true, completion: nil)
    } else {
      navigationController?.popViewController(animated: true)
    }
  }
  
}
