//
//  Identifiable.swift
//  AppetiserCodingChallenge
//
//  Created by "Team Appetiser" ( https://appetiser.com.au )
//  Copyright © 2019 "Appetiser Pty Ltd". All rights reserved.
//

typealias ModelId = Int

protocol Identifiable {
  var id: ModelId? { get }  
}

extension Equatable where Self: Identifiable {
  
  static func == (lhs: Self, rhs: Self) -> Bool {
    return lhs.id == rhs.id
  }
  
}
