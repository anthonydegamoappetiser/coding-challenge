//
//  Movie.swift
//  AppetiserCodingChallenge
//
//  Created by "Team Appetiser" ( https://appetiser.com.au )
//  Copyright © 2019 Appetiser Pty Ltd. All rights reserved.
//

import Foundation


class MovieCollectionSection: GenericCollectionSection<Movie> {}


struct Movie: APIModel, Identifiable, Codable, Equatable {
  let id: ModelId?
  let title: String?
  let artist: String?
  let artworkUrl: String?
  let price: Float?
  let genre: String?
  let longDescription: String?
  
  enum CodingKeys: String, CodingKey {
    case id = "trackId"
    case title = "trackName"
    case artist = "artistName"
    case artworkUrl = "artworkUrl100"
    case price = "trackPrice"
    case genre = "primaryGenreName"
    case longDescription
  }

  /// Larger thumbnail sizes for artwork.
  ///
  /// I didn't find any official documentation from Apple, but I got the idea from this
  /// [StackOverflow thread](https://stackoverflow.com/questions/13382208/getting-bigger-artwork-images-from-itunes-web-search).
  enum ThumbnailSize: Int {
    case medium = 300
    case large = 600
  }
}


extension Movie {
  /// Checks if this movie is marked as "Favorite".
  var isFavorite: Bool {
    guard let id = id else {
      return false
    }

    return FavoriteStore.getAll().contains(id)
  }


  /// Get the thumbnail URL for a specific size.
  ///
  /// The `artworkUrl` that is included on the iTunes Search API response is too small for our use case.
  /// Therefore, this method is created to generate a new URL with a specific resolution.
  ///
  /// - Parameter size: The size of the thumbnail.
  func thumbnailUrl(withSize size: Movie.ThumbnailSize) -> String? {
    guard
      let url = artworkUrl,
      let index = url.lastIndex(of: "/")
      else {
        return artworkUrl
    }

    return "\(url[..<index])/\(size.rawValue)x\(size.rawValue).jpg"
  }
}
