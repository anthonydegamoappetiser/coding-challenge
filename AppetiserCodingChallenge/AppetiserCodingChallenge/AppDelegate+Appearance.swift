//
//  AppDelegate+Appearance.swift
//  AppetiserCodingChallenge
//
//  Created by "Team Appetiser" ( https://appetiser.com.au )
//  Copyright © 2019 "Appetiser Pty Ltd". All rights reserved.
//

import UIKit

// All UIAppearance configurations should all go in here.

// MARK: - Appearance
extension AppDelegate {
  
  func loadAppearancePreferences() {
    loadNavBarAppearancePrefs()
    loadBarButtonItemAppearancePrefs()
    loadTableViewCellAppearancePrefs()
  }
  
  private func loadNavBarAppearancePrefs() {
    /*
    let navBar = UINavigationBar.appearance(whenContainedInInstancesOf: [...])
    navBar.barTintColor = ...
    navBar.tintColor = ...
    navBar.titleTextAttributes = [
      ...
    ]
    ...
    */
  }
  
  private func loadBarButtonItemAppearancePrefs() {
    /*
    let barButton = UIBarButtonItem.appearance(whenContainedInInstancesOf: [
      ...
    ])
    barButton.setTitleTextAttributes([
        ...
      ],
      for: .normal
    )
    barButton.setTitleTextAttributes([
        ...
      ],
      for: .selected
    )
    */
  }
  
  private func loadTableViewCellAppearancePrefs() {
    /*
    let cell = UITableViewCell.appearance(whenContainedInInstancesOf: [...])
    cell.tintColor = ...
    */
  }
  
}
