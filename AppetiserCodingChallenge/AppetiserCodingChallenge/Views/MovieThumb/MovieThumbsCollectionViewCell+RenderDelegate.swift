//
//  MovieThumbsCollectionViewCell+RenderDelegate.swift
//  AppetiserCodingChallenge
//
//  Created by Mark Anthony Degamo on 03/09/2019.
//  Copyright © 2019 Appetiser Pty Ltd. All rights reserved.
//

import UIKit


protocol MovieThumbsCollectionViewCellRenderDelegate: AnyObject {
  func collectionViewCell(_ cell: MovieThumbsCollectionViewCell, didRenderThumbForMovie movie: Movie)
}


extension MovieThumbsCollectionViewCellRenderDelegate {

  func loadThumbImage(
    forMovie movie: Movie,
    onCell cell: MovieThumbsCollectionViewCell,
    withSize size: Movie.ThumbnailSize
  ) {
    cell.thumbView.thumbnailImageView.image = nil
    cell.thumbView.activityIndicatorView.startAnimating()

    App.shared.api.fetchThumbnail(
      forMovie: movie,
      withSize: size,
      completion: { image in
        cell.thumbView.activityIndicatorView.stopAnimating()

        guard let image = image else {
          cell.thumbView.thumbnailImageView.image = R.image.brokenImage()
          return
        }

        cell.thumbView.thumbnailImageView.image = image
      })
  }

}
