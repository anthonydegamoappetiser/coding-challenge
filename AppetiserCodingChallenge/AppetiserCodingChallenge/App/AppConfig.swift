//
//  AppConfig.swift
//  AppetiserCodingChallenge
//
//  Created by "Team Appetiser" ( https://appetiser.com.au )
//  Copyright © 2019 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

struct AppConfig {
  
  // The maximum number of thumbnails to show on a single line collection
  static let maxThumbPerCollection = 10
  
}
