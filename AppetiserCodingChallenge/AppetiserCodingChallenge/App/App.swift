//
//  App.swift
//  AppetiserCodingChallenge
//
//  Created by "Team Appetiser" ( https://appetiser.com.au )
//  Copyright © 2018 "Appetiser Pty Ltd". All rights reserved.
//

import UIKit

/// This is our main application object. This holds instances of all the services available
/// in the app like the APIClient, AppUser, etc.
///
/// IMPORTANT:
/// - Defer creation of service instance up to the point where it's first needed.
///
class App {
  
  enum Environment: String {
    case staging
    case production
  }
  
  static let shared = App()
  
  static var environment: Environment {
    // These values are set in their corresponding Targets.
    // See: Target > Build Settings > Other Swift Flags.
    #if STAGING
      return .staging
    #else
      return .production
    #endif
  }
  
  private(set) var api: APIClient!
  
  let name = S.appName()

  // MARK: Initialization
  
  init() {
    debugLog("env: \(App.environment.rawValue)")
  }
  
  func bootstrap(
    with application: UIApplication,
    launchOptions: [UIApplication.LaunchOptionsKey: Any]?
  ) {
    api = APIClient(baseURL: URL(string: "https://itunes.apple.com")!)
  }
  
  func recordError(_ error: Error, info: [String: Any]? = nil) {
    debugLog(String(describing: error))
    
    if info != nil {
      debugLog("other info: \(String(describing: info!))")
    }
  }

}

protocol AppService {}

extension AppService {
  
  var app: App {
    return App.shared
  }
  
}

// MARK: - App Info
extension App {
  
  static var bundleIdentifier: String? {
    return Bundle.main.bundleIdentifier
  }
  
}
