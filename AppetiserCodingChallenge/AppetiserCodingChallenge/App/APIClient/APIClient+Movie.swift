//
//  APIClient+Movie.swift
//  AppetiserCodingChallenge
//
//  Created by "Team Appetiser" ( https://appetiser.com.au )
//  Copyright © 2019 Appetiser Pty Ltd. All rights reserved.
//

import Foundation
import Alamofire

extension APIClient {

  @discardableResult
  func getMovies(_ completion: @escaping APIClientMovieClosure) -> DataRequest {
    return request(
      "search",
      method: .get,
      parameters: [
        "term": "star",
        "country": "au",
        "media": "movie",
        "all": ""
      ],
      success: { resp in
        completion(resp.results, nil)
      },
      failure: { error in
        completion(nil, error)
      })
  }


  func fetchThumbnail(
    forMovie movie: Movie,
    withSize size: Movie.ThumbnailSize,
    completion: @escaping (UIImage?) -> Void
  ) {
    guard let artworkUrl = movie.artworkUrl else {
      completion(nil)
      return
    }

    guard let thumbnailUrl = movie.thumbnailUrl(withSize: size) else {
      getImage(
        from: artworkUrl,
        success: { image in completion(image) },
        failure: { _ in completion(nil) })

      return
    }

    getImage(
      from: thumbnailUrl,
      success: { image in completion(image) },
      failure: { _ in
        self.getImage(
          from: artworkUrl,
          success: { image in completion(image) },
          failure: { _ in completion(nil) })
      })
  }
  
}
