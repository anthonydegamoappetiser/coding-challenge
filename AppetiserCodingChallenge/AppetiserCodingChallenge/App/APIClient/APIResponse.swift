//
//  APIResponse.swift
//  AppetiserCodingChallenge
//
//  Created by "Team Appetiser" ( https://appetiser.com.au )
//  Copyright © 2018 "Appetiser Pty Ltd". All rights reserved.
//

import Foundation


/// An representation of the returned json from iTunes Search API
struct APIResponse: Decodable {
  let resultCount: Int
  let results: [Movie]
}
