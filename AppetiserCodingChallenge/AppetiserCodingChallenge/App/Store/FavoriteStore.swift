//
//  FavoriteStore.swift
//  AppetiserCodingChallenge
//
//  Created by "Team Appetiser" ( https://appetiser.com.au )
//  Copyright © 2019 Appetiser Pty Ltd. All rights reserved.
//

import Foundation


/// `UserDefaults` wrapper for persisting movies that are marked as *"Favorite"*.
enum FavoriteStore {
  private static let movieIdKey = "AppetiserCodingChallenge.UserDefaults.FavoriteStore.movieIdKey"
}


// MARK: - Methods
extension FavoriteStore {

  /// Get all movies marked as *"Favorite"*.
  ///
  /// - Returns: An array of `ModelId`
  static func getAll() -> [ModelId] {
    return (UserDefaults.standard.array(forKey: movieIdKey) as? [ModelId]) ?? [ModelId]()
  }


  /// Adds a movie to *"Favorites"*.
  ///
  /// - Note: This doesn't actually adds the `movie` to the *"Store"*; it just keeps a reference to it's `id`.
  ///
  /// - Parameter movie: The `movie` to add
  /// - Returns: `true` if successful `false` otherwise
  static func add(_ movie: Movie) -> Bool {
    guard let id = movie.id else {
      return false
    }

    var favorites = getAll()

    if !favorites.contains(id) {
      favorites.append(id)
      UserDefaults.standard.set(favorites, forKey: movieIdKey)
      UserDefaults.standard.synchronize()
      NotificationCenter.default.post(name: .onFavoriteStoreUpdated, object: nil)
      
      return true
    }
    
    return false
  }


  /// Removes a movie from *"Favorites"*.
  ///
  /// - Parameter movie: The `movie` to remove
  /// - Returns: returns `true` if successful `false` otherwise
  static func remove(_ movie: Movie) -> Bool {
    guard let id = movie.id else {
      return false
    }

    let favorites = getAll()
    
    if favorites.contains(id) {
      let newFavorites = favorites.filter { $0 != id }
      UserDefaults.standard.set(newFavorites, forKey: movieIdKey)
      UserDefaults.standard.synchronize()
      NotificationCenter.default.post(name: .onFavoriteStoreUpdated, object: nil)
      
      return true
    }
    
    return false
  }

}
