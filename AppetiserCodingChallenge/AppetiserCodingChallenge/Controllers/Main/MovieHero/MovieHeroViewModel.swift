//
//  MovieHeroViewModel.swift
//  AppetiserCodingChallenge
//
//  Created by "Team Appetiser" ( https://appetiser.com.au )
//  Copyright © 2019 Appetiser Pty Ltd. All rights reserved.
//

class MovieHeroViewModel {
  
  // Properties
  
  var data: Movie!
  
  var isFavorited: Bool {
    if let trackId = data.id,
      FavoriteStore.getAll().contains(trackId) {
      return true
    }
    
    return false
  }
  
}
