//
//  MovieCollectionCollectionView.swift
//  AppetiserCodingChallenge
//
//  Created by "Team Appetiser" ( https://appetiser.com.au )
//  Copyright © 2019 Appetiser Pty Ltd. All rights reserved.
//

import UIKit


class MovieCollectionCollectionView: UICollectionView {
  
  // MARK: - Properties
  
  var data: [Movie]!
  
  weak var actionsDelegate: MovieCollectionActionsDelegate?
  
  weak var renderDelegate: MovieCollectionRenderDelegate?
  
  let padding = CGFloat(16)

  
  // MARK: - Setup & Initialization
  
  override init(frame: CGRect, collectionViewLayout layout: UICollectionViewLayout) {
    super.init(frame: frame, collectionViewLayout: layout)
    setup()
  }
  
  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
    setup()
  }
  
  func setup() {
    backgroundColor = .clear
    showsHorizontalScrollIndicator = false
    showsVerticalScrollIndicator = false
    contentInset = UIEdgeInsets(top: padding * 2, left: padding, bottom: padding * 2, right: padding)
    dataSource = self
    delegate = self
  }
}


// MARK: - UICollectionViewDataSource

extension MovieCollectionCollectionView: UICollectionViewDataSource {
  func collectionView(
    _ collectionView: UICollectionView,
    numberOfItemsInSection section: Int
  ) -> Int {
    return data.count
  }

  
  func numberOfSections(in collectionView: UICollectionView) -> Int {
    return 1
  }


  func collectionView(
    _ collectionView: UICollectionView,
    cellForItemAt indexPath: IndexPath
  ) -> UICollectionViewCell {
    let cellIdentifier = R.reuseIdentifier.movieCollectionThumbCellIdentifier.identifier

    guard
      let cell = collectionView.dequeueReusableCell(
          withReuseIdentifier: cellIdentifier,
          for: indexPath
        ) as? MovieThumbsCollectionViewCell,
      let cellData = data[safe: indexPath.item]
      else {
        return UICollectionViewCell()
    }

    cell.data = cellData
    renderDelegate?.collectionViewCell(cell, didRenderThumbForMovie: cellData)
    
    return cell
  }
  
}


// MARK: - UICollectionViewDelegate

extension MovieCollectionCollectionView: UICollectionViewDelegate {
  func collectionView(
    _ collectionView: UICollectionView,
    didSelectItemAt indexPath: IndexPath
  ) {
    if let movie = data[safe: indexPath.item] {
      actionsDelegate?.didSelect(movie: movie)
    }
  }
}


// MARK: - UICollectionViewDelegateFlowLayout

extension MovieCollectionCollectionView: UICollectionViewDelegateFlowLayout {
  private func cellWidth(
    forContainer collectionView: UICollectionView,
    byItemCount count: Int
  ) -> CGFloat {
    let divisor = CGFloat(count)
    let sidePadding = padding * 2
    let totalGaps = padding * (divisor - 1)
    let availableScreenWidth = (collectionView.frame.size.width - (sidePadding + totalGaps))
    return availableScreenWidth / divisor
  }


  func collectionView(
    _ collectionView: UICollectionView,
    layout collectionViewLayout: UICollectionViewLayout,
    sizeForItemAt indexPath: IndexPath
  ) -> CGSize {
    let heightAddOn = CGFloat(50)
    var itemCount = 2

    if UIDevice.current.userInterfaceIdiom == .pad {
      itemCount = 4
      
      if UIDevice.current.orientation.isLandscape {
        itemCount = 6
      }
    } else {
      if UIDevice.current.orientation.isLandscape {
        itemCount = 3
      }
    }

    let width = cellWidth(forContainer: collectionView, byItemCount: itemCount)

    return CGSize(width: width, height: width + heightAddOn)
  }


  func collectionView(
    _ collectionView: UICollectionView,
    layout collectionViewLayout: UICollectionViewLayout,
    minimumInteritemSpacingForSectionAt section: Int
  ) -> CGFloat {
    return padding
  }


  func collectionView(
    _ collectionView: UICollectionView,
    layout collectionViewLayout: UICollectionViewLayout,
    minimumLineSpacingForSectionAt section: Int
  ) -> CGFloat {
    return padding * 2
  }
}
