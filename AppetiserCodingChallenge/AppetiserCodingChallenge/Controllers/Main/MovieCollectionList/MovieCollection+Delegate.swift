//
//  MovieCollection+Delegate.swift
//  AppetiserCodingChallenge
//
//  Created by Mark Anthony Degamo on 03/09/2019.
//  Copyright © 2019 Appetiser Pty Ltd. All rights reserved.
//

protocol MovieCollectionActionsDelegate: AnyObject {
  func didSelect(movie: Movie)
}

protocol MovieCollectionRenderDelegate: MovieThumbsCollectionViewCellRenderDelegate {}
