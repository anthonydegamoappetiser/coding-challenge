//
//  MovieCollectionListViewModel.swift
//  AppetiserCodingChallenge
//
//  Created by "Team Appetiser" ( https://appetiser.com.au )
//  Copyright © 2019 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

class MovieCollectionListViewModel {
  
  // MARK: - Properties
  
  var data: MovieCollectionSection!
  
  var hasFavorites: Bool {
    return FavoriteStore.getAll().count > 0
  }
  
}


// MARK: - Methods

extension MovieCollectionListViewModel {
  
  func refreshDataFromFavorites() {
    let favorites = FavoriteStore.getAll()
    let filteredMovies = data.items.filter { favorites.contains($0.id ?? -9999) }
    let newData = MovieCollectionSection()
    newData.headerTitle = data.headerTitle
    newData.items = filteredMovies
    data = newData
  }
  
}
