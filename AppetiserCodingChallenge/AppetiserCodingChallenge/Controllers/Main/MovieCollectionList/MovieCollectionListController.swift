//
//  MovieCollectionListController.swift
//  AppetiserCodingChallenge
//
//  Created by "Team Appetiser" ( https://appetiser.com.au )
//  Copyright © 2019 Appetiser Pty Ltd. All rights reserved.
//

import Foundation
import UIKit


class MovieCollectionListController: ViewController {
  
  // MARK: - Properties
  
  var viewModel = MovieCollectionListViewModel()
  
  
  // MARK: - Outlets
  
  @IBOutlet weak var mainCollectionView: MovieCollectionCollectionView! {
    didSet {
      mainCollectionView.renderDelegate = self
    }
  }
  
  
  // MARK: - Method Overrides
  
  override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    if let receiver = segue.destination as? MovieHeroController,
      let data = sender as? Movie {
      receiver.viewModel.data = data
    }
  }

}


// MARK: - Lifecycle

extension MovieCollectionListController {

  override func viewDidLoad() {
    super.viewDidLoad()

    initViewController()
    refreshCollectionView()
    setupNotificationObservers()
  }

  override func viewWillTransition(
    to size: CGSize,
    with coordinator: UIViewControllerTransitionCoordinator
  ) {
    super.viewWillTransition(to: size, with: coordinator)
    mainCollectionView.reloadData()
  }

}


// MARK: - Setup & Initialization

extension MovieCollectionListController {

  func initViewController() {
    self.navigationItem.title = viewModel.data.headerTitle
    mainCollectionView.actionsDelegate = self
  }


  func setupNotificationObservers() {
    let center = NotificationCenter.default

    center.addObserver(
      self,
      selector: #selector(didReceiveUpdateFavorites(notification:)),
      name: .onFavoriteStoreUpdated,
      object: nil)
  }

}


// MARK: - Methods

extension MovieCollectionListController {

  @objc func didReceiveUpdateFavorites(notification: NSNotification) {
    if viewModel.data.headerTitle != S.favorites() {
      return
    }
    
    if viewModel.hasFavorites {
      viewModel.refreshDataFromFavorites()
      refreshCollectionView()
    } else {
      navigationController?.popViewController(animated: true)
    }
  }
  
  
  func refreshCollectionView() {
    mainCollectionView.data = viewModel.data.items
    mainCollectionView.reloadData()
  }
  
}


// MARK: - MovieCollectionActionsDelegate

extension MovieCollectionListController: MovieCollectionActionsDelegate {
  
  func didSelect(movie: Movie) {
    performSegue(withIdentifier: R.segue.movieCollectionListController.toMovieHero.identifier, sender: movie)
  }
  
}


// MARK: - MovieCollectionRenderDelegate

extension MovieCollectionListController: MovieCollectionRenderDelegate {

  func collectionViewCell(
    _ cell: MovieThumbsCollectionViewCell,
    didRenderThumbForMovie movie: Movie
  ) {
    loadThumbImage(forMovie: movie, onCell: cell, withSize: .medium)
  }
  
}
