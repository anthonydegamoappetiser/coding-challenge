//
//  MovieList+Delegate.swift
//  AppetiserCodingChallenge
//
//  Created by Mark Anthony Degamo on 03/09/2019.
//  Copyright © 2019 Appetiser Pty Ltd. All rights reserved.
//


protocol MovieListTableViewCellActionsDelegate: MovieListThumbsActionsDelegate {
  func didEmitViewAllAction(moviesCollection: MovieCollectionSection)
}


protocol MovieListTableViewCellRenderDelegate: MovieListThumbsRenderDelegate {}


protocol MovieListThumbsActionsDelegate: AnyObject {
  func didSelectMovie(movie: Movie)
}


protocol MovieListThumbsRenderDelegate: MovieThumbsCollectionViewCellRenderDelegate {}


protocol MovieListTableViewActionsDelegate: MovieListTableViewCellActionsDelegate {}


protocol MovieListTableViewRenderDelegate: MovieListTableViewCellRenderDelegate {}
