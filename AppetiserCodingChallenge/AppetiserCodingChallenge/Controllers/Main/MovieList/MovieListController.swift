//
//  HomeController.swift
//  AppetiserCodingChallenge
//
//  Created by "Team Appetiser" ( https://appetiser.com.au )
//  Copyright © 2018 "Appetiser Pty Ltd". All rights reserved.
//

import UIKit

class MovieListController: ViewController {
  
  // MARK: - Properties
  
  var viewModel = MovieListViewModel()
  
  
  // MARK: - Outlets
  
  @IBOutlet weak var mainTableView: MovieListTableView!
  
  
  // MARK: - Method Overrides
  
  override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    if let receiver = segue.destination as? MovieCollectionListController,
      let data = sender as? MovieCollectionSection {
      receiver.viewModel.data = data
    } else if let receiver = segue.destination as? MovieHeroController,
      let data = sender as? Movie {
      receiver.viewModel.data = data
    }
  }

}


// MARK: - Lifecycle

extension MovieListController {

  override func viewDidLoad() {
    super.viewDidLoad()

    initViewController()
    setupNotificationObservers()
    refreshTableView()
  }


  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    navigationController?.setNavigationBarHidden(false, animated: true)
  }

}


// MARK: - Setup & Initialization

extension MovieListController {

  func initViewController() {
    navigationItem.title = App.shared.name

    mainTableView.actionsDelegate = self
    mainTableView.renderDelegate = self
  }
  
  
  func setupNotificationObservers() {
    let center = NotificationCenter.default

    center.addObserver(
      self,
      selector: #selector(didReceiveUpdateFavorites(notification:)),
      name: .onFavoriteStoreUpdated,
      object: nil)
  }

}


// MARK: - Methods

extension MovieListController {

  @objc func didReceiveUpdateFavorites(notification: NSNotification) {
    refreshTableView()
  }
  
  
  func refreshTableView() {
    viewModel.fetchMovies { movieCollection in
      if movieCollection.isEmpty {
        return
      }
      
      self.mainTableView.data = movieCollection
      self.mainTableView.reloadData()
    }
  }
  
}


// MARK: - MovieListTableViewActionsDelegate

extension MovieListController: MovieListTableViewActionsDelegate {
  
  func didEmitViewAllAction(moviesCollection: MovieCollectionSection) {
    performSegue(withIdentifier: R.segue.movieListController.toMovieCollection.identifier, sender: moviesCollection)
  }
  
  func didSelectMovie(movie: Movie) {
    performSegue(withIdentifier: R.segue.movieListController.toMovieHero.identifier, sender: movie)
  }
  
}


// MARK: - MovieListTableViewRenderDelegate

extension MovieListController: MovieListTableViewRenderDelegate {

  func collectionViewCell(
    _ cell: MovieThumbsCollectionViewCell,
    didRenderThumbForMovie movie: Movie
  ) {
    loadThumbImage(forMovie: movie, onCell: cell, withSize: .medium)
  }

}
