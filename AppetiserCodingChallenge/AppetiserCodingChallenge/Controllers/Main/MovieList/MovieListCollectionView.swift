//
//  MovieListCollectionView.swift
//  AppetiserCodingChallenge
//
//  Created by "Team Appetiser" ( https://appetiser.com.au )
//  Copyright © 2019 Appetiser Pty Ltd. All rights reserved.
//

import UIKit


class MovieListCollectionView: UICollectionView {
  
  // MARK: Properties
  
  var data: [Movie]!
  
  weak var actionsDelegate: MovieListThumbsActionsDelegate?
  
  weak var renderDelegate: MovieListThumbsRenderDelegate?


  // MARK: - Setup & Initialization

  override init(frame: CGRect, collectionViewLayout layout: UICollectionViewLayout) {
    super.init(frame: frame, collectionViewLayout: layout)
    setup()
  }


  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
    setup()
  }


  func setup() {
    backgroundColor = .clear
    clipsToBounds = false
    showsHorizontalScrollIndicator = false
    showsVerticalScrollIndicator = false
    contentInset = UIEdgeInsets(top: 0, left: 16, bottom: 0, right: 16)
    dataSource = self
    delegate = self
  }

}


// MARK: - UICollectionViewDataSource

extension MovieListCollectionView: UICollectionViewDataSource {
  
  func collectionView(
    _ collectionView: UICollectionView,
    numberOfItemsInSection section: Int
  ) -> Int {
    let max = AppConfig.maxThumbPerCollection
    return data.count > max ? max : data.count
  }
  
  
  func numberOfSections(in collectionView: UICollectionView) -> Int {
    return 1
  }
  
  
  func collectionView(
    _ collectionView: UICollectionView,
    cellForItemAt indexPath: IndexPath
  ) -> UICollectionViewCell {
    let cellIdentifier = R.reuseIdentifier.movieListThumbCellIdentifier.identifier

    guard
      let cell = collectionView.dequeueReusableCell(
        withReuseIdentifier: cellIdentifier,
        for: indexPath) as? MovieThumbsCollectionViewCell,
      let cellData = data?[safe: indexPath.item]
      else {
        return UICollectionViewCell()
    }
    
    cell.data = cellData
    renderDelegate?.collectionViewCell(cell, didRenderThumbForMovie: cellData)

    return cell
  }
  
}


// MARK: - UICollectionViewDelegate

extension MovieListCollectionView: UICollectionViewDelegate {
  
  func collectionView(
    _ collectionView: UICollectionView,
    didSelectItemAt indexPath: IndexPath
  ) {
    if let movie = data[safe: indexPath.item] {
      actionsDelegate?.didSelectMovie(movie: movie)
    }
  }
  
}


// MARK: - UICollectionViewDelegateFlowLayout

extension MovieListCollectionView: UICollectionViewDelegateFlowLayout {
  
  func collectionView(
    _ collectionView: UICollectionView,
    layout collectionViewLayout: UICollectionViewLayout,
    sizeForItemAt indexPath: IndexPath
  ) -> CGSize {
    return CGSize(width: 130, height: 180)
  }
  
  
  func collectionView(
    _ collectionView: UICollectionView,
    layout collectionViewLayout: UICollectionViewLayout,
    minimumInteritemSpacingForSectionAt section: Int
  ) -> CGFloat {
    return CGFloat(10)
  }
  
}
