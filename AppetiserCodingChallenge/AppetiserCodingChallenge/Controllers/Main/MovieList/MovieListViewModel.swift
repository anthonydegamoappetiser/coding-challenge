//
//  HomeViewModel.swift
//  AppetiserCodingChallenge
//
//  Created by "Team Appetiser" ( https://appetiser.com.au )
//  Copyright © 2019 Appetiser Pty Ltd. All rights reserved.
//

import Foundation


class MovieListViewModel {
  
  func fetchMovies(_ completion: @escaping ([MovieCollectionSection]) -> Void) {
    App.shared.api.getMovies { movies, error in
      guard let movies = movies, error == nil else {
        completion([])
        return
      }
      
      var collection = [MovieCollectionSection]()
      
      let favoriteMovies = self.getFavoriteMovies(movies)
      if favoriteMovies.items.count > 0 {
        collection.append(favoriteMovies)
      }
      
      let featuredMovies = self.getFeaturedMovies(movies)
      collection.append(featuredMovies)
      
      let genreGroup = self.groupMoviesByGenre(movies)
      collection.append(contentsOf: genreGroup)
      
      completion(collection)
    }
  }
  
  
  func getFavoriteMovies(_ movies: [Movie]) -> MovieCollectionSection {
    let favoriteIds = FavoriteStore.getAll()
    let favoriteMovies = movies.filter {
      favoriteIds.contains($0.id ?? -9999)
    }
    
    let movieCollection = MovieCollectionSection()
    movieCollection.headerTitle = S.favorites()
    movieCollection.items = favoriteMovies

    return movieCollection
  }
  
  
  func getFeaturedMovies(_ movies: [Movie]) -> MovieCollectionSection {
    var featuredMovies = [Movie]()
    
    repeat {
      let index = Int.random(in: 0 ..< movies.count)
      let movie = movies[index]
      
      if !featuredMovies.contains(where: { $0.id == movie.id }) {
        featuredMovies.append(movie)
      }
    } while (featuredMovies.count < AppConfig.maxThumbPerCollection)
    
    let collection = MovieCollectionSection()
    collection.headerTitle = S.featured()
    collection.items = featuredMovies
    
    return collection
  }
  
  
  func groupMoviesByGenre(_ movies: [Movie]) -> [MovieCollectionSection] {
    var collection = [MovieCollectionSection]()
    
    for movie in movies {
      let genre = movie.genre ?? R.string.localizable.unknown()
      
      if let index = collection.firstIndex(where: { $0.headerTitle == genre }) {
        collection[index].items.append(movie)
      } else {
        let newCollection = MovieCollectionSection()
        newCollection.headerTitle = genre
        newCollection.items = [movie]
        collection.append(newCollection)
      }
    }
    
    return collection.sorted {
      switch ($0.headerTitle, $1.headerTitle) {
      case let(l?, r?):
        return l < r
      case (nil, _):
        return false
      case (_?, nil):
        return true
      }
    }
  }
  
}
