//
//  HomeTableViewCell.swift
//  AppetiserCodingChallenge
//
//  Created by "Team Appetiser" ( https://appetiser.com.au )
//  Copyright © 2019 Appetiser Pty Ltd. All rights reserved.
//

import UIKit


class MovieListTableViewCell: UITableViewCell {
  
  // MARK: - Properties
  
  var data: MovieCollectionSection! {
    didSet {
      sectionLabel.text = data.headerTitle
      thumbsCollectionView.data = data.items
      thumbsCollectionView.reloadData()
    }
  }
  
  weak var actionsDelegate: MovieListTableViewCellActionsDelegate?
  
  weak var renderDelegate: MovieListTableViewCellRenderDelegate?
  
  
  // MARK: - Outlets
  
  @IBOutlet weak var sectionLabel: UILabel! {
    didSet {
      sectionLabel.font = .boldSystemFont(ofSize: 18)
    }
  }
  
  @IBOutlet weak var viewAllButton: UIButton! {
    didSet {
      viewAllButton.setTitle(S.viewAll(), for: .normal)
      viewAllButton.titleLabel?.font = UIFont.systemFont(ofSize: 13)
    }
  }
  
  @IBOutlet weak var thumbsCollectionView: MovieListCollectionView! {
    didSet {
      thumbsCollectionView.actionsDelegate = self
      thumbsCollectionView.renderDelegate = self
    }
  }

}


// MARK: - Actions

extension MovieListTableViewCell {

  @IBAction fileprivate func didTapViewAllButton(_ sender: Any) {
    actionsDelegate?.didEmitViewAllAction(moviesCollection: data)
  }

}


// MARK: - MovieListThumbsActionsDelegate

extension MovieListTableViewCell: MovieListThumbsActionsDelegate {
  
  func didSelectMovie(movie: Movie) {
    actionsDelegate?.didSelectMovie(movie: movie)
  }
  
}


// MARK: - MovieListTableViewCellRenderDelegate

extension MovieListTableViewCell: MovieListTableViewCellRenderDelegate {

  func collectionViewCell(
    _ cell: MovieThumbsCollectionViewCell,
    didRenderThumbForMovie movie: Movie
  ) {
    renderDelegate?.collectionViewCell(cell, didRenderThumbForMovie: movie)
  }
  
}
