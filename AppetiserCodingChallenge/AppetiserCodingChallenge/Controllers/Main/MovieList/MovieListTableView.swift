//
//  MovieListTableView.swift
//  AppetiserCodingChallenge
//
//  Created by "Team Appetiser" ( https://appetiser.com.au )
//  Copyright © 2019 Appetiser Pty Ltd. All rights reserved.
//

import UIKit


class MovieListTableView: UITableView {
  
  // MARK: - Properties
  
  var data = [MovieCollectionSection]()
  
  weak var actionsDelegate: MovieListTableViewActionsDelegate?
  
  weak var renderDelegate: MovieListTableViewRenderDelegate?
  
  
  // MARK: - Setup & Initialization
  
  override init(frame: CGRect, style: UITableView.Style) {
    super.init(frame: frame, style: style)
    setup()
  }
  
  
  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
    setup()
  }
  
  
  func setup() {
    dataSource = self
    separatorStyle = .none
    allowsSelection = false
  }
  
}


// MARK: - UITableViewDataSource

extension MovieListTableView: UITableViewDataSource {
  
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return data.count
  }
  
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cellIdentifier = R.reuseIdentifier.movieListTableViewCellIdentifier.identifier
    guard
      let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as? MovieListTableViewCell,
      let cellData = data[safe: indexPath.item]
      else {
        return UITableViewCell()
    }
    
    cell.data = cellData
    cell.actionsDelegate = self
    cell.renderDelegate = self
    return cell
  }
  
}


// MARK: - MovieListTableViewCellActionsDelegate

extension MovieListTableView: MovieListTableViewCellActionsDelegate {
  
  func didEmitViewAllAction(moviesCollection: MovieCollectionSection) {
    actionsDelegate?.didEmitViewAllAction(moviesCollection: moviesCollection)
  }
  
  
  func didSelectMovie(movie: Movie) {
    actionsDelegate?.didSelectMovie(movie: movie)
  }
  
}


// MARK: - MovieListTableViewRenderDelegate

extension MovieListTableView: MovieListTableViewRenderDelegate {

  func collectionViewCell(
    _ cell: MovieThumbsCollectionViewCell,
    didRenderThumbForMovie movie: Movie
  ) {
    renderDelegate?.collectionViewCell(cell, didRenderThumbForMovie: movie)
  }
  
}
