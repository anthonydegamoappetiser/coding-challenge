//
//  AppDelegate.swift
//  AppetiserCodingChallenge
//
//  Created by "Team Appetiser" ( https://appetiser.com.au )
//  Copyright © 2018 "Appetiser Pty Ltd". All rights reserved.
//

import UIKit
import AlamofireNetworkActivityIndicator

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
  
  var window: UIWindow?
  
  override init() {}
  
  func application(
    _ application: UIApplication,
    didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]? = nil
  ) -> Bool {
    App.shared.bootstrap(with: application, launchOptions: launchOptions)
    
    NetworkActivityIndicatorManager.shared.isEnabled = true
    
    window = UIWindow(frame: UIScreen.main.bounds)
    window?.backgroundColor = UIColor.black
    window?.tintColor = Styles.Colors.windowTint
    updateRootViewController()
    loadAppearancePreferences()
    window?.makeKeyAndVisible()
    
    return true
  }
}


// MARK: - RootViewController Management
extension AppDelegate {
  func updateRootViewController() {
    self.window?.setRootViewControllerAnimated(R.storyboard.main.instantiateInitialViewController())
  }
}
